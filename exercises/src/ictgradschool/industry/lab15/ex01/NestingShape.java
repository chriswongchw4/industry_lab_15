package ictgradschool.industry.lab15.ex01;

import java.util.*;

/**
 * Created by chw4 on 8/05/2017.
 */
public class NestingShape extends Shape {

    List<Shape> nestingShape = new ArrayList<>();

    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (Iterator<Shape> it = nestingShape.iterator(); it.hasNext(); ) {
            it.next().move(fWidth, fHeight);
        }
    }

    public void add(Shape child) throws IllegalArgumentException {

        if (child.parent != null) {
            throw new IllegalArgumentException();
        } else if (child.getHeight() + child.getY() > this.fHeight || child.getWidth() + child.getX() > this.fWidth) {
            throw new IllegalArgumentException();
        } else {
            this.nestingShape.add(child);
            child.parent = this;
        }
    }

    public void remove(Shape child) {
        this.nestingShape.remove(child);
        child.parent = null;
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > nestingShape.size()) {
            throw new IndexOutOfBoundsException();
        }
        return nestingShape.get(index);
    }

    public int shapeCount() {
        return nestingShape.size();
    }

    public int indexOf(Shape child) {
        if (nestingShape.contains(child)) {
            return nestingShape.indexOf(child);
        }
        return -1;
    }

    public boolean contains(Shape child) {
        return nestingShape.contains(child);
    }

    @Override
    public void paint(Painter painter) {

        painter.drawRect(fX, fY, fWidth, fHeight);
        painter.translate(fX, fY);

        for (Shape s : nestingShape){
            s.paint(painter);
        }

        painter.translate(-fX,-fY);
    }


}
