package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * Created by chw4 on 8/05/2017.
 */
public class TreeModelAdapter implements TreeModel {

    private NestingShape _adaptee;

    public TreeModelAdapter(NestingShape root){
        _adaptee=root;
    }

    @Override
    public Object getRoot() { // todo
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) { // todo
       Object result = null;

       if (parent instanceof NestingShape){
           NestingShape nestingShape = (NestingShape)parent;
           result = nestingShape.shapeAt(index);
       }

        return result;
    }

    @Override
    public int getChildCount(Object parent) { // todo
        int result = 0;
        Shape shape = (Shape) parent;

        if (shape instanceof NestingShape) {
            NestingShape nestingShape = (NestingShape) shape;
            result = nestingShape.shapeCount();
        }

        return result;
    }

    @Override
    public boolean isLeaf(Object node) { // todo
        return !(node instanceof NestingShape);
    }



    @Override
    public int getIndexOfChild(Object parent, Object child) { // todo
        int indexOfChild = -1;

        if (parent instanceof  NestingShape){
            NestingShape nestingShape = (NestingShape)parent;
            Shape shape = (Shape)child;
            indexOfChild = nestingShape.indexOf(shape);
        }

        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

}
